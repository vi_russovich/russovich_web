package ru.liga.homefinance.service.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;
import ru.liga.homefinance.service.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ParentCategoryServiceTest {

    @InjectMocks
    private ParentCategoryService parentCategoryService;
    private ParentCategoryRepository parentCategoryRepository = Mockito.mock(ParentCategoryRepository.class);

    @Test
    void testParentCategoryServiceClass() {
        when(parentCategoryRepository.save(any())).thenReturn(null);

        ParentCategoryModel parentCategoryModel = fillingParentCategoryModel();

        String expected = parentCategoryModel.getName();
        String actual = parentCategoryService.info(parentCategoryModel).getName();
        Assert.assertEquals(expected, actual);
    }

    private ParentCategoryModel fillingParentCategoryModel (){
        return new ParentCategoryModel("Пополнение в терминале");
    }


}
