package ru.liga.homefinance.service.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;
import ru.liga.homefinance.service.*;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;
    private AccountUsersRepository accountUsersRepository = Mockito.mock(AccountUsersRepository.class);
    private CurrencyRepository currencyRepository = Mockito.mock(CurrencyRepository.class);

    @Test
    void testAccountServiceClass() {
        when(accountUsersRepository.save(any())).thenReturn(null);
        doNothing().when(currencyRepository).update(any());

        AccountUsersModel accountUsersModel = fillingAccountUsersModel();

        String expected = accountUsersModel.getCurrencyModel().getSymbol();
        String actual = accountService.info(accountUsersModel).getCurrencyModel().getSymbol();
        Assert.assertEquals(expected, actual);
    }

    private AccountUsersModel fillingAccountUsersModel (){

        CurrencyRepository currencyRepository = new CurrencyRepository(new ConnectionSupplier());
        CurrencyModel currencyModel =currencyRepository.findByID(1);
        BigDecimal bigDecimal = new BigDecimal("1000000");
        return new AccountUsersModel("Emily", bigDecimal, currencyModel);
    }

}
