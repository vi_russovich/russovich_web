package ru.liga.homefinance.service.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;
import ru.liga.homefinance.service.*;


import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TransactionTypeServiceTest {

    @InjectMocks
    private TransactionTypeService transactionTypeService;
    private TransactionTypeRepository transactionTypeRepository = Mockito.mock(TransactionTypeRepository.class);

    @Test
    void testTransactionTypeServiceClass() {
        when(transactionTypeRepository.save(any())).thenReturn(null);

        TransactionType transactionType = fillingTransactionType();

        String expected = transactionType.getName();
        String actual = transactionTypeService.info(transactionType).getName();
        Assert.assertEquals(expected, actual);
    }

    private TransactionType fillingTransactionType (){
        return new TransactionType("Пополнение");
    }

}
