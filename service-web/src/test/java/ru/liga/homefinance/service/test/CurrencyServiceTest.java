package ru.liga.homefinance.service.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;
import ru.liga.homefinance.service.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceTest {

    @InjectMocks
    private CurrencyService currencyService;
    private CurrencyRepository currencyRepository = Mockito.mock(CurrencyRepository.class);

    @Test
    void testCurrencyServiceClass() {
        when(currencyRepository.save(any())).thenReturn(null);

        CurrencyModel currencyModel = fillingCurrencyModel();

        String expected = currencyModel.getCode();
        String actual = currencyService.info(currencyModel).getCode();
        Assert.assertEquals(expected, actual);
    }

    private CurrencyModel fillingCurrencyModel (){
        return new CurrencyModel("Юань", "4", "¥");
    }



}
