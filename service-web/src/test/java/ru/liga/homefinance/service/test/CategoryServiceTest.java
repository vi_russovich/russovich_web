package ru.liga.homefinance.service.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;
import ru.liga.homefinance.service.*;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    @InjectMocks
    private CategoryService categoryService;
    private CategoryRepository categoryRepository = Mockito.mock(CategoryRepository.class);
    private ParentCategoryRepository parentCategoryRepository = Mockito.mock(ParentCategoryRepository.class);
    private TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);

    @Test
    void testCategoryServiceClass() {
        when(categoryRepository.save(any())).thenReturn(null);
        doNothing().when(parentCategoryRepository).update(any());
        doNothing().when(transactionRepository).update(any());

        CategoryModel categoryModel = fillingCategoryModel();

        String expected = categoryModel.getParent_id().getName();
        String actual = categoryService.info(categoryModel).getParent_id().getName();
        Assert.assertEquals(expected, actual);
    }

    private CategoryModel fillingCategoryModel(){

        ParentCategoryRepository parentCategoryRepository = new ParentCategoryRepository(new ConnectionSupplier());
        TransactionRepository transactionRepository = new TransactionRepository(new ConnectionSupplier());
        Set<TransactionModel> transactionModels = new HashSet<>();
        ParentCategoryModel parentCategoryModel = parentCategoryRepository.findByID(2);
        TransactionModel transactionModel1 = transactionRepository.findByID(3);
        TransactionModel transactionModel2 = transactionRepository.findByID(4);
        transactionModel1.setCategoryModels(null);
        transactionModel2.setCategoryModels(null);
        transactionModels.add(transactionModel1);
        transactionModels.add(transactionModel2);
        return new CategoryModel("Гостиничные услуги", parentCategoryModel, transactionModels);
    }

}
