package ru.liga.homefinance.service.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;
import ru.liga.homefinance.service.*;

import java.util.HashSet;
import java.util.Set;
import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @InjectMocks
    private TransactionService transactionService;
    private TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
    private TransactionTypeRepository transactionTypeRepository = Mockito.mock(TransactionTypeRepository.class);
    private AccountUsersRepository accountUsersRepository = Mockito.mock(AccountUsersRepository.class);
    private CategoryRepository categoryRepository = Mockito.mock(CategoryRepository.class);

    @Test
    void testTransactionServiceClass() {
        when(transactionRepository.save(any())).thenReturn(null);
        doNothing().when(transactionTypeRepository).update(any());
        doNothing().when(accountUsersRepository).update(any());
        doNothing().when(categoryRepository).update(any());

        TransactionModel transactionModel = fillingTransactionModel();

        BigDecimal expected = transactionModel.getAccountUsersModel().getAmount();
        BigDecimal actual = transactionService.info(transactionModel).getAccountUsersModel().getAmount();
        Assert.assertEquals(expected, actual);
    }

    private TransactionModel fillingTransactionModel (){
    AccountUsersRepository accountUsersRepository = new AccountUsersRepository(new ConnectionSupplier());
    CategoryRepository categoryRepository = new CategoryRepository(new ConnectionSupplier());
    TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(new ConnectionSupplier());
    BigDecimal bigDecimal = new BigDecimal("100");
    Set<CategoryModel> categoryModels = new HashSet<>();
    CategoryModel categoryModel1 = categoryRepository.findByID(1);
    CategoryModel categoryModel2 = categoryRepository.findByID(2);
        categoryModel1.setTransactionModels(null);
        categoryModel2.setTransactionModels(null);
        categoryModels.add(categoryModel1);
        categoryModels.add(categoryModel2);
        return new TransactionModel("Перевод Тестовый", bigDecimal,false, transactionTypeRepository.findByID(1),accountUsersRepository.findByID(1),categoryModels);
}

}
