package ru.liga.homefinance.service;

import ru.liga.homefinance.dao.model.TransactionType;
import ru.liga.homefinance.dao.repository.ConnectionSupplier;
import ru.liga.homefinance.dao.repository.TransactionTypeRepository;

import java.util.ArrayList;

public class TransactionTypeService {

    private TransactionTypeRepository transactionTypeRepository;

    public TransactionTypeService() {
        this.transactionTypeRepository = new TransactionTypeRepository(new ConnectionSupplier());
    }
    public TransactionType findById(Long id) {
        return transactionTypeRepository.findByID(id);
    }
    public ArrayList<TransactionType> findAll() {
        return transactionTypeRepository.findAll();
    }
    public TransactionType save(TransactionType transactionType){
        return transactionTypeRepository.save(transactionType);
    }
    public void delete(Long id) {
        transactionTypeRepository.remove(id);
    }
    public TransactionType info(TransactionType transactionType){return  transactionType;}
}
