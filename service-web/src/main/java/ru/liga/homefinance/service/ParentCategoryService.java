package ru.liga.homefinance.service;

import ru.liga.homefinance.dao.model.ParentCategoryModel;
import ru.liga.homefinance.dao.repository.ConnectionSupplier;
import ru.liga.homefinance.dao.repository.ParentCategoryRepository;

import java.util.ArrayList;

public class ParentCategoryService {
    private ParentCategoryRepository parentCategoryRepository;

    public ParentCategoryService() {
        this.parentCategoryRepository = new ParentCategoryRepository(new ConnectionSupplier());
    }
    public ParentCategoryModel findById(Long id) {
        return parentCategoryRepository.findByID(id);
    }
    public ArrayList<ParentCategoryModel> findAll() {
        return parentCategoryRepository.findAll();
    }
    public ParentCategoryModel save(ParentCategoryModel parentCategoryModel){
        return parentCategoryRepository.save(parentCategoryModel);
    }
    public void delete(Long id) {
        parentCategoryRepository.remove(id);
    }
    public ParentCategoryModel info(ParentCategoryModel parentCategoryModel){return parentCategoryModel;}
}
