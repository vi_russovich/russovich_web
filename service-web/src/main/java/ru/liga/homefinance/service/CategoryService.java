package ru.liga.homefinance.service;

import ru.liga.homefinance.dao.model.CategoryModel;
import ru.liga.homefinance.dao.repository.CategoryRepository;
import ru.liga.homefinance.dao.repository.ConnectionSupplier;

import java.util.ArrayList;

public class CategoryService {
    private CategoryRepository categoryRepository;

    public CategoryService() {
        this.categoryRepository = new CategoryRepository(new ConnectionSupplier());
    }
    public CategoryModel findById(Long id) {
        return categoryRepository.findByID(id);
    }
    public ArrayList<CategoryModel> findAll() {
        return categoryRepository.findAll();
    }
    public CategoryModel save(CategoryModel categoryModel){
        return categoryRepository.save(categoryModel);
    }
    public void delete(Long id) {
        categoryRepository.remove(id);
    }
    public CategoryModel findByIdNonTransactionModels(Long id){
        return categoryRepository.findByIdNonTransactionModels(id);
    }
    public CategoryModel info(CategoryModel categoryModel){return categoryModel;}
}
