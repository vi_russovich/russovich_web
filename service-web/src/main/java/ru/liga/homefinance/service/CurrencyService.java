package ru.liga.homefinance.service;

import ru.liga.homefinance.dao.model.CurrencyModel;
import ru.liga.homefinance.dao.repository.CurrencyRepository;
import ru.liga.homefinance.dao.repository.ConnectionSupplier;

import java.util.ArrayList;

public class CurrencyService {
    private CurrencyRepository currencyRepository;

    public CurrencyService() {
        this.currencyRepository = new CurrencyRepository(new ConnectionSupplier());
    }
    public CurrencyModel findById(Long id) {
        return currencyRepository.findByID(id);
    }
    public CurrencyModel findByName(String name) {
        return currencyRepository.findByName(name);
    }
    public ArrayList<CurrencyModel> findAll() {
        return currencyRepository.findAll();
    }
    public CurrencyModel save(CurrencyModel currencyModel){
        return currencyRepository.save(currencyModel);
    }
    public void delete(Long id) {
        currencyRepository.remove(id);
    }
    public CurrencyModel info(CurrencyModel currencyModel){return currencyModel;};
}
