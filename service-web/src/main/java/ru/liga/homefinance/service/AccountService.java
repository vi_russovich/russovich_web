package ru.liga.homefinance.service;

import ru.liga.homefinance.dao.model.AccountUsersModel;
import ru.liga.homefinance.dao.repository.AccountUsersRepository;
import ru.liga.homefinance.dao.repository.ConnectionSupplier;

import java.util.ArrayList;

public class AccountService {
    private AccountUsersRepository accountRepository;

    public AccountService() {
        this.accountRepository = new AccountUsersRepository(new ConnectionSupplier());
    }
    public AccountUsersModel findById(Long id) {
        return accountRepository.findByID(id);
    }
    public ArrayList<AccountUsersModel> findAll() {
        return accountRepository.findAll();
    }
    public AccountUsersModel save(AccountUsersModel accountUsersModel){
        return accountRepository.save(accountUsersModel);
    }
    public void delete(Long id) {
        accountRepository.remove(id);
    }
    public AccountUsersModel info(AccountUsersModel accountUsersModel){return accountUsersModel;}
}
