package ru.liga.homefinance.service;

import ru.liga.homefinance.dao.model.TransactionModel;
import ru.liga.homefinance.dao.repository.TransactionRepository;
import ru.liga.homefinance.dao.repository.ConnectionSupplier;

import java.util.ArrayList;

public class TransactionService {

    private TransactionRepository transactionRepository;

    public TransactionService() {
        this.transactionRepository = new TransactionRepository(new ConnectionSupplier());
    }
    public TransactionModel findById(Long id) {
        return transactionRepository.findByID(id);
    }
    public ArrayList<TransactionModel> findAll() {
        return transactionRepository.findAll();
    }
    public TransactionModel save(TransactionModel transactionModel){
        return transactionRepository.save(transactionModel);
    }
    public void delete(Long id) {
        transactionRepository.remove(id);
    }

    public TransactionModel findByIdNonTransactionModels(Long id){
        return transactionRepository.findByIdNonCategoryModels(id);
    }
    public TransactionModel info(TransactionModel transactionModel){return transactionModel;}
}
