package ru.liga.homefinance.dao.test;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.liga.homefinance.dao.HomeFinanceException;
import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;

import java.math.BigDecimal;
import java.lang.Boolean;
import java.util.HashSet;
import java.util.Set;

public class TransactionRepositoryTest {
    private TransactionRepository transactionRepository;
    private CategoryRepository categoryRepository;
    private TransactionTypeRepository transactionTypeRepository;
    private AccountUsersRepository accountUsersRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        transactionRepository = new TransactionRepository(connectionSupplier);
        categoryRepository = new CategoryRepository(connectionSupplier);
        transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
        accountUsersRepository = new AccountUsersRepository(connectionSupplier);
    }

    private TransactionModel fillingTransactionCategoryModel (){

        Set<CategoryModel> categoryModels = new HashSet<>();
        AccountUsersModel accountUsersModel = accountUsersRepository.findByID(1);
        TransactionType transactionType = transactionTypeRepository.findByID(1);
        CategoryModel categoryModel1 = categoryRepository.findByID(1);
        CategoryModel categoryModel2 = categoryRepository.findByID(2);
        categoryModel1.setTransactionModels(null);
        categoryModel2.setTransactionModels(null);
        categoryModels.add(categoryModel1);
        categoryModels.add(categoryModel2);
        return new TransactionModel("Тест1", BigDecimal.valueOf(700), Boolean.TRUE, transactionType, accountUsersModel, categoryModels);
    }

    @Test
    public void testTransactionRepositoryFindByIdAndSave() {
        TransactionModel expected = fillingTransactionCategoryModel();

        expected.setId(transactionRepository.save(expected).getId());
        TransactionModel actual = transactionRepository.findByID(expected.getId());

        Assert.assertEquals(expected,actual);

        transactionRepository.remove(actual.getId());
    }

    @Test
    public void testTransactionRepositoryUpdate() {
        TransactionModel expected = fillingTransactionCategoryModel();
        expected = transactionRepository.save(expected);
        expected.setName("Стипендия");
        transactionRepository.update(expected);

        Assert.assertEquals("Стипендия", transactionRepository.findByID
                (expected.getId()).getName());
//        Assert.assertEquals(transactionRepository.findByID(expected.getId()).getTransactionType(), transactionRepository.findByID
//                (expected.getId()).getTransactionType());
//        Assert.assertEquals(transactionRepository.findByID(expected.getId()).getAccountUsersModel(), transactionRepository.findByID
//                (expected.getId()).getAccountUsersModel());
//        Assert.assertEquals(transactionRepository.findByID(expected.getId()).getCategoryModels(), transactionRepository.findByID
//                (expected.getId()).getCategoryModels());

        transactionRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testTransactionRepositoryRemove() {
        TransactionModel expected = fillingTransactionCategoryModel();
        expected = transactionRepository.save(expected);

        transactionRepository.remove(expected.getId());
        transactionRepository.findByID(expected.getId());
    }


}
