package ru.liga.homefinance.dao.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.liga.homefinance.dao.HomeFinanceException;
import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;

public class CurrencyRepositoryTest {

    private CurrencyRepository currencyRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        currencyRepository = new CurrencyRepository(connectionSupplier);
    }

    private CurrencyModel fillingCurrency (){
        return new CurrencyModel("Тест", "4", "¥");
    }

    @Test
    public void testCurrencyRepositoryFindByIdAndSave() {
        CurrencyModel expected = fillingCurrency();

        expected.setId(currencyRepository.save(expected).getId());
        CurrencyModel actual = currencyRepository.findByID(expected.getId());

        Assert.assertEquals(expected,actual);

        currencyRepository.remove(actual.getId());
    }

    @Test
    public void testCurrencyRepositoryUpdate() {
        CurrencyModel expected = fillingCurrency();
        expected = currencyRepository.save(expected);
        expected.setName("Юань");
        currencyRepository.update(expected);

        Assert.assertEquals("Юань", currencyRepository.findByID
                (expected.getId()).getName());
        currencyRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testCurrencyRepositoryRemove() {
        CurrencyModel expected = fillingCurrency();
        expected = currencyRepository.save(expected);

        currencyRepository.remove(expected.getId());
        currencyRepository.findByID(expected.getId());
    }





}
