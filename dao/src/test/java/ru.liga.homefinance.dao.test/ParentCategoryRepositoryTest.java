package ru.liga.homefinance.dao.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.liga.homefinance.dao.HomeFinanceException;
import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;


public class ParentCategoryRepositoryTest {
    private ParentCategoryRepository parentCategoryRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        parentCategoryRepository = new ParentCategoryRepository(connectionSupplier);
    }


    private ParentCategoryModel fillingParentCategory(){

        return new ParentCategoryModel("Тест");
    }


    @Test
    public void testParentCategoryRepositoryFindByIdAndSave() {
        ParentCategoryModel expected = fillingParentCategory();

        expected.setId(parentCategoryRepository.save(expected).getId());
        ParentCategoryModel actual = parentCategoryRepository.findByID(expected.getId());

        Assert.assertEquals(expected,actual);

        parentCategoryRepository.remove(actual.getId());
    }


    @Test
    public void testParentCategoryRepositoryUpdate() {
        ParentCategoryModel expected = fillingParentCategory();
        expected = parentCategoryRepository.save(expected);
        expected.setName("Мы поменяли на кафе и рестораны");
        parentCategoryRepository.update(expected);

        Assert.assertEquals("Мы поменяли на кафе и рестораны", parentCategoryRepository.findByID
                (expected.getId()).getName());
        parentCategoryRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testParentCategoryRepositoryRemove() {
        ParentCategoryModel expected = fillingParentCategory();
        expected = parentCategoryRepository.save(expected);

        parentCategoryRepository.remove(expected.getId());
        parentCategoryRepository.findByID(expected.getId());
    }



}
