package ru.liga.homefinance.dao.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.liga.homefinance.dao.HomeFinanceException;
import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;


public class TransactionTypeRepositoryTest {
    private TransactionTypeRepository transactionTypeRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
    }


    private TransactionType fillingTransactionType (){
        return new TransactionType("Тест");
    }


    @Test
    public void testTransactionTypeRepositoryFindByIdAndSave() {
        TransactionType expected = fillingTransactionType();

        expected.setId(transactionTypeRepository.save(expected).getId());
        TransactionType actual = transactionTypeRepository.findByID(expected.getId());

        Assert.assertEquals(expected,actual);

        transactionTypeRepository.remove(actual.getId());
    }

    @Test
    public void testTransactionTypeRepositoryUpdate() {
        TransactionType expected = fillingTransactionType();
        expected = transactionTypeRepository.save(expected);
        expected.setName("Мы поменяли на Swift переводы");
        transactionTypeRepository.update(expected);

        Assert.assertEquals("Мы поменяли на Swift переводы", transactionTypeRepository.findByID
                (expected.getId()).getName());
        transactionTypeRepository.remove(expected.getId());
    }


    @Test(expected = HomeFinanceException.class)
    public void testTransactionTypeRepositoryRemove() {
        TransactionType expected = fillingTransactionType();
        expected = transactionTypeRepository.save(expected);

        transactionTypeRepository.remove(expected.getId());
        transactionTypeRepository.findByID(expected.getId());
    }



}
