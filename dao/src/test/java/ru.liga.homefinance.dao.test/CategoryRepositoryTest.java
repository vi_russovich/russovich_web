package ru.liga.homefinance.dao.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.liga.homefinance.dao.HomeFinanceException;
import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;

import java.util.HashSet;
import java.util.Set;

    public class CategoryRepositoryTest {
        private CategoryRepository categoryRepository;
        private TransactionRepository transactionRepository;
        private ParentCategoryRepository parentCategoryRepository;

        @Before
        public void setUp() {
            ConnectionSupplier connectionSupplier = new ConnectionSupplier();
            categoryRepository = new CategoryRepository(connectionSupplier);
            transactionRepository = new TransactionRepository(connectionSupplier);
            parentCategoryRepository = new ParentCategoryRepository(connectionSupplier);
        }

        private CategoryModel fillingCategoryTransactionModel (){

            Set<TransactionModel> transactionModels = new HashSet<>();
            ParentCategoryModel parentCategoryModel = parentCategoryRepository.findByID(2);
            TransactionModel transactionModel1 = transactionRepository.findByID(1);
            TransactionModel transactionModel2 = transactionRepository.findByID(2);
            transactionModel1.setCategoryModels(null);
            transactionModel2.setCategoryModels(null);
            transactionModels.add(transactionModel1);
            transactionModels.add(transactionModel2);
            return new CategoryModel("Тест", parentCategoryModel, transactionModels);
        }

        @Test
        public void testCategoryRepositoryFindByIdAndSave() {
            CategoryModel expected = fillingCategoryTransactionModel();

            expected.setId(categoryRepository.save(expected).getId());
            CategoryModel actual = categoryRepository.findByID(expected.getId());

            Assert. assertEquals(expected,actual);

            categoryRepository.remove(actual.getId());
        }

        @Test
        public void testCategoryRepositoryUpdate() {
            CategoryModel expected = fillingCategoryTransactionModel();
            expected = categoryRepository.save(expected);
            expected.setNameCategory("Корм для животных");
            categoryRepository.update(expected);

            Assert.assertEquals("Корм для животных", categoryRepository.findByID
                    (expected.getId()).getNameCategory());
//            Assert.assertEquals(categoryRepository.findByID(expected.getId()).getParent_id(), categoryRepository.findByID
//                    (expected.getId()).getParent_id());
//            Assert.assertEquals(categoryRepository.findByID(expected.getId()).getTransactionModels(), categoryRepository.findByID
//                    (expected.getId()).getTransactionModels());

            categoryRepository.remove(expected.getId());
        }

        @Test(expected = HomeFinanceException.class)
        public void testCategoryRepositoryRemove() {
            CategoryModel expected = fillingCategoryTransactionModel();
            expected = categoryRepository.save(expected);

            categoryRepository.remove(expected.getId());
            categoryRepository.findByID(expected.getId());
        }
    }


