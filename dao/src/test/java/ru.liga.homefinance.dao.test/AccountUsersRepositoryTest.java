package ru.liga.homefinance.dao.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.liga.homefinance.dao.HomeFinanceException;
import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.dao.repository.*;

import java.math.BigDecimal;

public class AccountUsersRepositoryTest {
    private AccountUsersRepository accountUsersRepository;
    private CurrencyRepository currencyRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        accountUsersRepository = new AccountUsersRepository(connectionSupplier);
        currencyRepository = new CurrencyRepository(connectionSupplier);
    }

    private AccountUsersModel fillingAccountCurrency (){

        CurrencyModel currencyModel = currencyRepository.findByID(2);

        return new AccountUsersModel("Тест", BigDecimal.valueOf(1000000) , currencyModel);
    }


    @Test
    public void testAccountUsersRepositoryFindByIdAndSave() {
        AccountUsersModel expected = fillingAccountCurrency();

        expected.setId(accountUsersRepository.save(expected).getId());
        AccountUsersModel actual = accountUsersRepository.findByID(expected.getId());

        Assert.assertEquals(expected,actual);

        accountUsersRepository.remove(actual.getId());
    }


    @Test
    public void testAccountUsersRepositoryUpdate() {
        AccountUsersModel expected = fillingAccountCurrency();
        expected = accountUsersRepository.save(expected);
        expected.setName("Мы поменяли на Daniel");
        accountUsersRepository.update(expected);

        Assert.assertEquals("Мы поменяли на Daniel", accountUsersRepository.findByID
                (expected.getId()).getName());
        Assert.assertEquals(accountUsersRepository.findByID(expected.getId()).getCurrencyModel(), accountUsersRepository.findByID
                (expected.getId()).getCurrencyModel());
        accountUsersRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testAccountUsersRepositoryRemove() {
        AccountUsersModel expected = fillingAccountCurrency();
        expected = accountUsersRepository.save(expected);

        accountUsersRepository.remove(expected.getId());
        accountUsersRepository.findByID(expected.getId());
    }

}
