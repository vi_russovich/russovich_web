package ru.liga.homefinance.dao.repository;


import ru.liga.homefinance.dao.model.*;

import java.math.BigDecimal;
import java.sql.SQLInput;
import java.sql.SQLType;
import java.util.HashSet;
import java.util.Set;


public class Main {

    public static void main(String[] args) {

        ConnectionSupplier connect = new ConnectionSupplier();
        CurrencyRepository currencyRepository = new CurrencyRepository(connect);
        TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(connect);
        ParentCategoryRepository parentCategoryRepository = new ParentCategoryRepository(connect);
        AccountUsersRepository accountUsersRepository = new AccountUsersRepository(connect);
        TransactionRepository transactionRepository = new TransactionRepository(connect);
        TransactionCategoryRepository transactionCategoryRepository = new TransactionCategoryRepository(connect);
        CategoryRepository categoryRepository = new CategoryRepository(connect);


//        System.out.println(transactionTypeRepository.findByID(1));
//        System.out.println(parentCategoryRepository.findAll());
//        System.out.println(accountUsersRepository.findAll());
        System.out.println(transactionRepository.findByID(2));
//        transactionCategoryRepository.save(2, 1);
//        System.out.println(categoryRepository.findByID(13));
//          transactionCategoryRepository.remove(1);


    }

}
