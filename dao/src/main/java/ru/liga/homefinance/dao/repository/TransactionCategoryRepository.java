package ru.liga.homefinance.dao.repository;

import ru.liga.homefinance.dao.HomeFinanceException;
import ru.liga.homefinance.dao.model.*;

import java.sql.*;
import java.util.Set;
import java.util.HashSet;

public class TransactionCategoryRepository {


    private ConnectionSupplier connectionSupplier;

    public TransactionCategoryRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    public void save(long transaction_id, long category_id) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_category_tbl (transaction_id, category_id) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS)){
                preparedStatement.setLong(1, transaction_id);
                preparedStatement.setLong(2, category_id);

                preparedStatement.executeUpdate();

                connection.commit();

            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in save TransactionCategoryRepository", e);
            }

        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save TransactionCategoryRepository", e);
        }
    }


    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_category_tbl WHERE transaction_id = " + id)){

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in remove TransactionCategoryRepository", e);
            }
        } catch (SQLException e){
            throw new HomeFinanceException("Error in remove TransactionCategoryRepository", e);
        }
    }


    public Set<CategoryModel> findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_category_tbl WHERE transaction_id = " + "'" + id + "'")) {

                Set<CategoryModel> categoryModels = new HashSet<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while(resultSet.next()){
                    categoryModels.add(findByIDCategory(resultSet.getLong(2)));
                }

                return categoryModels;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID TransactionCategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID TransactionCategoryRepository", e);
        }
    }

    public CategoryModel findByIDCategory(long id){

            try (Connection connection = connectionSupplier.connection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement
                        ("SELECT * FROM categoryModel_tbl WHERE id = " + "'" + id + "'")) {

                    CategoryModel categoryModel = new CategoryModel();
                    ParentCategoryRepository parentCategoryRepository = new ParentCategoryRepository(connectionSupplier);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    resultSet.next();

                    categoryModel.setId(resultSet.getLong(1));
                    categoryModel.setNameCategory(resultSet.getString(2));
                    categoryModel.setParent_id(parentCategoryRepository.findByID(resultSet.getLong(3)));

                    return categoryModel;
                } catch (SQLException e) {
                    throw new HomeFinanceException("Error in findByID CategoryRepository", e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID CategoryRepository", e);
            }
        }

    public void update(long transaction_id, long category_id) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transaction_category_tbl SET category_id = " + "'" + category_id + "'" +
                            "WHERE transaction_id = " + "'" + transaction_id + "'")) {


                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in update TransactionCategoryRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in update TransactionCategoryRepository", e);
        }
    }




}
