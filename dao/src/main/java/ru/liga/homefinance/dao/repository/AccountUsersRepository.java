package ru.liga.homefinance.dao.repository;

import ru.liga.homefinance.dao.*;
import ru.liga.homefinance.dao.model.*;
import java.sql.*;
import java.util.ArrayList;

public class AccountUsersRepository implements Repository<AccountUsersModel>{

    private ConnectionSupplier connectionSupplier;

    public AccountUsersRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public AccountUsersModel save(AccountUsersModel model) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO accountModel_tbl (name, amount, currency_id) VALUES (?,?,?)",Statement.RETURN_GENERATED_KEYS)){
                CurrencyRepository currencyRepository = new CurrencyRepository(connectionSupplier);
                preparedStatement.setString(1, model.getName());
                preparedStatement.setBigDecimal(2, model.getAmount());
                preparedStatement.setLong(3, model.getCurrencyModel().getId());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in save AccountUsersRepository", e);
            }

        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save AccountUsersRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM accountModel_tbl WHERE id = " + id)){

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in remove AccountUsersRepository", e);
            }
        } catch (SQLException e){
            throw new HomeFinanceException("Error in remove AccountUsersRepository", e);
        }
    }

    @Override
    public AccountUsersModel findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl WHERE id = " + "'" + id + "'")) {

                AccountUsersModel accountUsersModel = new AccountUsersModel();
                CurrencyRepository currencyRepository = new CurrencyRepository(connectionSupplier);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                accountUsersModel.setId(resultSet.getLong(1));
                accountUsersModel.setName(resultSet.getString(2));
                accountUsersModel.setAmount(resultSet.getBigDecimal(3));
                accountUsersModel.setCurrencyModel(currencyRepository.findByID(resultSet.getLong(4)));
                return accountUsersModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID AccountUsersRepository", e);
        }
    }

    @Override
    public void update(AccountUsersModel model) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE accountModel_tbl SET name = " + "'" + model.getName() + "'" +
                                    ", amount = " + "'" + model.getAmount() + "'" +
                                    ", currency_id = " + "'" + model.getCurrencyModel().getId() + "'" +
                            "WHERE ID = " + model.getId()))  {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in update AccountUsersRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in update AccountUsersRepository", e);
        }
    }

    @Override
    public ArrayList<AccountUsersModel> findAll() {
        try(Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountModel_tbl")) {

                ArrayList<AccountUsersModel> accountUsersModels = new ArrayList<>();
                CurrencyRepository currencyRepository = new CurrencyRepository(connectionSupplier);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    AccountUsersModel accountUsersModel = new AccountUsersModel();
                    accountUsersModel.setId(resultSet.getLong(1));
                    accountUsersModel.setName(resultSet.getString(2));
                    accountUsersModel.setAmount(resultSet.getBigDecimal(3));
                    accountUsersModel.setCurrencyModel(currencyRepository.findByID(resultSet.getLong(4)));


                    accountUsersModels.add(accountUsersModel);
                }
                return accountUsersModels;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findAll AccountUsersRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in findAll AccountUsersRepository", e);
        }
    }

}
