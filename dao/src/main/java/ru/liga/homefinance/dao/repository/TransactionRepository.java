package ru.liga.homefinance.dao.repository;

import ru.liga.homefinance.dao.*;
import ru.liga.homefinance.dao.model.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public class TransactionRepository implements Repository<TransactionModel>{

    private ConnectionSupplier connectionSupplier;
    private TransactionCategoryRepository transactionCategoryRepository;

    public TransactionRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        this.transactionCategoryRepository = new TransactionCategoryRepository(connectionSupplier);
    }

    @Override
    public TransactionModel save(TransactionModel model) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transactionModel_tbl (name, amount, profitOrLoss, type_id, account_id) VALUES (?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS)){

                preparedStatement.setString(1, model.getName());
                preparedStatement.setBigDecimal(2, model.getAmount());
                preparedStatement.setBoolean(3, model.getProfitOrLoss());
                preparedStatement.setLong(4, model.getTransactionType().getId());
                preparedStatement.setLong(5, model.getAccountUsersModel().getId());

                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
//                return model;
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in save TransactionRepository", e);
            }

        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save TransactionRepository", e);
        }
        Iterator<CategoryModel> it = model.getCategoryModels().iterator();
        while (it.hasNext()) {
            transactionCategoryRepository.save(model.getId(), it.next().getId());
        }
        return model;
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()){
            transactionCategoryRepository.remove(id);
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transactionModel_tbl WHERE id = " + id)){

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in remove TransactionRepository", e);
            }
        } catch (SQLException e){
            throw new HomeFinanceException("Error in remove TransactionRepository", e);
        }
    }

    @Override
    public TransactionModel findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionModel_tbl WHERE id = " + "'" + id + "'")) {

                TransactionModel transactionModel = new TransactionModel();
                TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
                AccountUsersRepository accountUsersRepository = new AccountUsersRepository(connectionSupplier);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();

                transactionModel.setId(resultSet.getLong(1));
                transactionModel.setName(resultSet.getString(2));
                transactionModel.setAmount(resultSet.getBigDecimal(3));
                transactionModel.setProfitOrLoss(resultSet.getBoolean(4));
                transactionModel.setTransactionType(transactionTypeRepository.findByID(resultSet.getLong(5)));
                transactionModel.setAccountUsersModel(accountUsersRepository.findByID(resultSet.getLong(6)));
                transactionModel.setCategoryModels(transactionCategoryRepository.findByID(resultSet.getLong(1)));

                return transactionModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID TransactionRepository", e);
        }
    }

    @Override
    public void update(TransactionModel model) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transactionModel_tbl SET name = " + "'" + model.getName() + "'" +
                            ", amount = " + "'" + model.getAmount() + "'" +
                            ", profitOrLoss = " + "'" + Boolean.compare(model.getProfitOrLoss(), false) + "'" +
                            ", type_id = " + "'" + model.getTransactionType().getId() + "'" +
                            ", account_id = " + "'" + model.getAccountUsersModel().getId() + "'" +
                            "WHERE ID = " + model.getId()))  {
                transactionCategoryRepository.remove(model.getId());
                Iterator<CategoryModel> it = model.getCategoryModels().iterator();
                while (it.hasNext()) {
                    transactionCategoryRepository.save(model.getId(), it.next().getId());
                }

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in update TransactionRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in update TransactionRepository", e);
        }
    }

    @Override
    public ArrayList<TransactionModel> findAll() {
        try(Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionModel_tbl")) {

                ArrayList<TransactionModel> transactionModels = new ArrayList<>();
                TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
                AccountUsersRepository accountUsersRepository = new AccountUsersRepository(connectionSupplier);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    TransactionModel transactionModel = new TransactionModel();
                    transactionModel.setId(resultSet.getLong(1));
                    transactionModel.setName(resultSet.getString(2));
                    transactionModel.setAmount(resultSet.getBigDecimal(3));
                    transactionModel.setProfitOrLoss(resultSet.getBoolean(4));
                    transactionModel.setTransactionType(transactionTypeRepository.findByID(resultSet.getLong(5)));
                    transactionModel.setAccountUsersModel(accountUsersRepository.findByID(resultSet.getLong(6)));
                    transactionModel.setCategoryModels(transactionCategoryRepository.findByID(resultSet.getLong(1)));



                    transactionModels.add(transactionModel);
                }
                return transactionModels;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findAll TransactionRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in findAll TransactionRepository", e);
        }
    }

    public TransactionModel findByIdNonCategoryModels(long id){
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM  transactionModel_tbl " + "WHERE id = " + "'" + id + "'")) {

                TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
                AccountUsersRepository accountUsersRepository = new AccountUsersRepository(connectionSupplier);
                TransactionModel transactionModel = new TransactionModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                transactionModel.setId(resultSet.getLong(1));
                transactionModel.setName(resultSet.getString(2));
                transactionModel.setAmount(resultSet.getBigDecimal(3));
                transactionModel.setProfitOrLoss(resultSet.getBoolean(4));
                transactionModel.setTransactionType(transactionTypeRepository.findByID(resultSet.getLong(5)));
                transactionModel.setAccountUsersModel(accountUsersRepository.findByID(resultSet.getLong(6)));
                return transactionModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findById TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findById TransactionRepository", e);
        }
    }

}
