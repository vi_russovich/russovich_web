package ru.liga.homefinance.dao.repository;

import ru.liga.homefinance.dao.*;
import ru.liga.homefinance.dao.model.*;
import java.sql.*;
import java.util.ArrayList;

public class ParentCategoryRepository implements Repository<ParentCategoryModel>{


    private ConnectionSupplier connectionSupplier;

    public ParentCategoryRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public ParentCategoryModel save(ParentCategoryModel model) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO parentCategoryModel_tbl (name) VALUES (?)",Statement.RETURN_GENERATED_KEYS)){

                preparedStatement.setString(1, model.getName());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in save ParentCategoryRepository", e);
            }

        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save ParentCategoryRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM parentCategoryModel_tbl WHERE id = " + id)){

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in remove ParentCategoryRepository", e);
            }
        } catch (SQLException e){
            throw new HomeFinanceException("Error in remove ParentCategoryRepository", e);
        }
    }

    @Override
    public ParentCategoryModel findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parentCategoryModel_tbl WHERE id = " + "'" + id + "'")) {

                ParentCategoryModel parentCategoryModel = new ParentCategoryModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                parentCategoryModel.setId(resultSet.getLong(1));
                parentCategoryModel.setName(resultSet.getString(2));
                return parentCategoryModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID ParentCategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID ParentCategoryRepository", e);
        }
    }

    @Override
    public void update(ParentCategoryModel model) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE parentCategoryModel_tbl SET name = " + "'" + model.getName() + "'" +
                            "WHERE ID = " + model.getId()))  {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in update ParentCategoryRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in update ParentCategoryRepository", e);
        }
    }

    @Override
    public ArrayList<ParentCategoryModel> findAll() {
        try(Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parentCategoryModel_tbl")) {

                ArrayList<ParentCategoryModel> parentCategoryModels = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    ParentCategoryModel parentCategoryModel = new ParentCategoryModel();
                    parentCategoryModel.setId(resultSet.getLong(1));
                    parentCategoryModel.setName(resultSet.getString(2));

                    parentCategoryModels.add(parentCategoryModel);
                }
                return parentCategoryModels;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findAll ParentCategoryRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in findAll ParentCategoryRepository", e);
        }
    }

}
