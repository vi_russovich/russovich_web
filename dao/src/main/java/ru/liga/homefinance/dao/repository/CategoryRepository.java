package ru.liga.homefinance.dao.repository;


import ru.liga.homefinance.dao.*;
import ru.liga.homefinance.dao.model.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public class CategoryRepository implements Repository<CategoryModel> {

    private ConnectionSupplier connectionSupplier;
    private CategoryTransactionRepository categoryTransactionRepository;

    public CategoryRepository(ConnectionSupplier connectionSupplier) {
                this.connectionSupplier = connectionSupplier;
                this.categoryTransactionRepository = new CategoryTransactionRepository(connectionSupplier);
    }


    @Override
    public CategoryModel save(CategoryModel model) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO categoryModel_tbl (name, parent_id) VALUES (?,?)",Statement.RETURN_GENERATED_KEYS)){


                CategoryTransactionRepository categoryTransactionRepository = new CategoryTransactionRepository(connectionSupplier);
                preparedStatement.setString(1, model.getNameCategory());
                preparedStatement.setLong(2, model.getParent_id().getId());

                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
//                return model;
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in save CategoryRepository", e);
            }

        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save CategoryRepository", e);
        }

        Iterator<TransactionModel> it = model.getTransactionModels().iterator();
        while (it.hasNext()) {
            categoryTransactionRepository.save(model.getId(), it.next().getId());
        }
        return model;
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()){
            categoryTransactionRepository.remove(id);
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM categoryModel_tbl WHERE id = " + id)){

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in remove CategoryRepository", e);
            }
        } catch (SQLException e){
            throw new HomeFinanceException("Error in remove CategoryRepository", e);
        }
    }

    @Override
    public CategoryModel findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl WHERE id = " + "'" + id + "'")) {

                CategoryModel categoryModel = new CategoryModel();
                ParentCategoryRepository parentCategoryRepository = new ParentCategoryRepository(connectionSupplier);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();

                categoryModel.setId(resultSet.getLong(1));
                categoryModel.setNameCategory(resultSet.getString(2));
                categoryModel.setParent_id(parentCategoryRepository.findByID(resultSet.getLong(3)));
                categoryModel.setTransactionModels(categoryTransactionRepository.findByID(resultSet.getLong(1)));

                return categoryModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID CategoryRepository", e);
        }
    }

    @Override
    public void update(CategoryModel model) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE categoryModel_tbl SET name = " + "'" + model.getNameCategory() + "'" +
                            ", parent_id = " + "'" + model.getParent_id().getId() + "'" +
                            "WHERE ID = " + model.getId()))  {
                categoryTransactionRepository.remove(model.getId());
                Iterator<TransactionModel> it = model.getTransactionModels().iterator();
                while (it.hasNext()) {
                    categoryTransactionRepository.save(model.getId(), it.next().getId());
                }
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in update CategoryRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in update CategoryRepository", e);
        }
    }

    @Override
    public ArrayList<CategoryModel> findAll() {
        try(Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categoryModel_tbl")) {

                ArrayList<CategoryModel> categoryModels = new ArrayList<>();
                ParentCategoryRepository parentCategoryRepository = new ParentCategoryRepository(connectionSupplier);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    CategoryModel categoryModel = new CategoryModel();
                    categoryModel.setId(resultSet.getLong(1));
                    categoryModel.setNameCategory(resultSet.getString(2));
                    categoryModel.setParent_id(parentCategoryRepository.findByID(resultSet.getLong(3)));
                    categoryModel.setTransactionModels(categoryTransactionRepository.findByID(resultSet.getLong(1)));

                    categoryModels.add(categoryModel);
                }
                return categoryModels;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findAll CategoryRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in findAll CategoryRepository", e);
        }
    }

    public CategoryModel findByIdNonTransactionModels(long id){
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM  categoryModel_tbl " + "WHERE id = " + "'" + id + "'")) {

                ParentCategoryRepository parentCategoryRepository = new ParentCategoryRepository(connectionSupplier);
                CategoryModel categoryModel = new CategoryModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                categoryModel.setId(resultSet.getLong(1));
                categoryModel.setNameCategory(resultSet.getString(2));
                categoryModel.setParent_id(parentCategoryRepository.findByID(resultSet.getLong(3)));
                return categoryModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findById CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findById CategoryRepository", e);
        }
    }



}
