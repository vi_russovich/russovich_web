<%--
  Created by IntelliJ IDEA.
  User: vrussovich
  Date: 14.10.2020
  Time: 18:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="ru.liga.homefinance.dao.model.TransactionModel" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="bank.ico">
    <title>Transactions</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 5rem;
        }

        h1{
            text-align: center;
        }
        .starter-template {
            padding: 3rem 1.5rem;
            text-align: center;
        }

        .all-forms{
            padding: 15px;
            margin: 0 auto;
            width: 900px;
            border: solid #f7f7f9;
            position: relative
        }
        p{
            text-align: center;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <%--                <a class="nav-link" href="${pageContext.request.contextPath}/ShowTransactionServlet"> All Transaction <span class="sr-only">(current)</span></a>--%>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Transaction actions</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/TransactionServlet">New Transaction</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/ShowTransactionServlet">All Transactions</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div>
    <h1>Add Transaction</h1>
</div>

<div>
    <%
        TransactionModel transactionModel = (TransactionModel) request.getAttribute("transaction");
        if (transactionModel != null) {
            out.println("<p>Transaction added!</p>");

        }
    %>
    </div>

        <div class="all-forms">
        <form method="post">
            <div class="form-group row">
            <label class="col-sm-3">Name:</label>
                <input type="text" class="form-control col-sm-8" name="name">
            </div>
            <div class="form-group row">
            <label class="col-sm-3">Amount:</label>
                <input type="text" class="form-control col-sm-8" name="amount">
            </div>
            <div class="form-group row">
            <label class="col-sm-3">Profit or loss:</label>
                <input type="text" class="form-control col-sm-8" name="profitOrLoss">
            </div>
            <div class="form-group row">
            <label class="col-sm-3">TypeTransaction ID:</label>
                <input type="text" class="form-control col-sm-8" name="transactionType">
            </div>
            <div class="form-group row">
            <label class="col-sm-3">Account ID:</label>
                <input type="text" class="form-control col-sm-8" name="account">
            </div>
            <div class="form-group row">
            <label class="col-sm-3">Category:</label>
                <input type="text" class="form-control col-sm-8" name="category">
            </div>
            <button type="submit" class="btn btn-dark">Submit</button>
        </form>
    </div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

</body>
<%request.setCharacterEncoding("UTF-8");%>
</html>
