<%@ page import="ru.liga.homefinance.service.TransactionService" %>
<%@ page import="ru.liga.homefinance.dao.model.TransactionModel" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: vrussovich
  Date: 15.10.2020
  Time: 2:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="bank.ico">
    <title>Transactions</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <style>
        body {
            padding-top: 5rem;
        }

        h1{
            text-align: center;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <%--                <a class="nav-link" href="${pageContext.request.contextPath}/ShowTransactionServlet"> All Transaction <span class="sr-only">(current)</span></a>--%>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Transaction actions</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/TransactionServlet">New Transaction</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/ShowTransactionServlet">All Transactions</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div>
    <h1>Transactions</h1>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Amount</th>
        <th scope="col">Profit or loss</th>
        <th scope="col">Transaction type</th>
        <th scope="col">Account</th>
        <th scope="col">Category</th>
    </tr>
    </thead>
    <tbody>
    <%
        TransactionService transactionService = new TransactionService();
        ArrayList<TransactionModel> transactionModels = transactionService.findAll();
        for(TransactionModel transactionModel: transactionModels){
            out.println("<tr>");
            out.println("<td>" + transactionModel.getName() + "</td>");
            out.println("<td>"+ transactionModel.getAmount()+"</td>");
            out.println("<td>"+ transactionModel.getProfitOrLoss()+"</td>");
            out.println("<td>"+ transactionModel.getTransactionType().getName()+"</td>");
            out.println("<td>"+ transactionModel.getAccountUsersModel().getName()+ "," + transactionModel.getAccountUsersModel().getAmount()+"\n"+
                    transactionModel.getAccountUsersModel().getCurrencyModel().getName()+","+ transactionModel.getAccountUsersModel().getCurrencyModel().getCode()+","+
                    transactionModel.getAccountUsersModel().getCurrencyModel().getSymbol() +"</td>");
            out.println("<td>"+ transactionModel.getCategoryModels()+"</td>");
            out.println("</tr>");
        }
    %>
    </tbody>
</table>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

</body>
<%request.setCharacterEncoding("UTF-8");%>
</html>
