package ru.liga.homefinance.web.servlet;

import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.service.AccountService;
import ru.liga.homefinance.service.CategoryService;
import ru.liga.homefinance.service.TransactionService;
import ru.liga.homefinance.service.TransactionTypeService;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.sql.Connection;
import java.sql.DriverManager;

@WebServlet("/TransactionServlet")
public class TransactionServlet extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        Set<CategoryModel> categoryModels = new HashSet<>();
        TransactionService transactionService = new TransactionService();
        TransactionTypeService transactionTypeService = new TransactionTypeService();
        AccountService accountService = new AccountService();
        CategoryService categoryService = new CategoryService();

        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setName(req.getParameter("name"));
        transactionModel.setAmount(new BigDecimal(req.getParameter("amount")));
        transactionModel.setProfitOrLoss(Boolean.parseBoolean(req.getParameter("profitOrLoss")));
        transactionModel.setTransactionType(transactionTypeService.findById(Long.parseLong(req.getParameter("transactionType"))));
        transactionModel.setAccountUsersModel(accountService.findById(Long.parseLong(req.getParameter("account"))));
        categoryModels.add(categoryService.findByIdNonTransactionModels(Long.parseLong(req.getParameter("category"))));

        transactionModel.setCategoryModels(categoryModels);
        transactionService.save(transactionModel);
        req.setAttribute("transaction", transactionModel);
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/new_java_web_russovich", "root", "K7l8M9n5_pwd");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("jsp/transactionJSP.jsp");
        requestDispatcher.forward(req, resp);

    }

}
