package ru.liga.homefinance.web.servlet;


import ru.liga.homefinance.dao.model.*;
import ru.liga.homefinance.service.AccountService;
import ru.liga.homefinance.service.CategoryService;
import ru.liga.homefinance.service.TransactionService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.DriverManager;

@WebServlet("/ShowTransactionServlet")
public class ShowTransactionServlet extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = resp.getWriter();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/new_java_web_russovich?useSSL=false&serverTimezone=UTC", "root", "K7l8M9n5_pwd");
//            TransactionService transactionService = new TransactionService();
//            ArrayList<TransactionModel> transactionModels = transactionService.findAll();
//
//
//            for(TransactionModel transactionModel: transactionModels){
//                pw.println(transactionModel);
//            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("jsp/showTransactionsJSP.jsp");
        requestDispatcher.forward(req, resp);
    }
    }
